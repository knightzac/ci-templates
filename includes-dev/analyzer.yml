# When using dind, it's wise to use the overlayfs driver for
# improved performance.
variables:
  # The context of these variables is global to the downstream job
  DOCKER_DRIVER: overlay2
  MAJOR: 1
  TMP_IMAGE: $CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA
  GO_VERSION: 1.15.0

stages:
  - maintenance      # update the project dependencies; only if MAINTENANCE is set
  - pre-build        # lint code, run unit tests, and compile binaries
  - build-package    # build distro package(s) for the analyzer and/or its dependencies
  - build-image      # build the Docker image(s) for the analyzer
  - test             # check, test, and scan the Docker images
  - performance-metrics # run performance checks
  - release-version  # release Docker images and distro packages
  - release-major    # update Docker images and distro packages of the major release
  - post             # run benchmarks

.if-gitlab-org-branch: &if-gitlab-org-branch
  if: '$CI_PROJECT_NAMESPACE == "gitlab-org/security-products/analyzers"'

# Only run for gitlab-org branches, as downstream needs exec permissions
.downstream_rules: &downstream_rules
  - <<: *if-gitlab-org-branch
  - when: never

.maintenance:
  stage: maintenance
  rules:
    - if: $MAINTENANCE
      when: always
    - when: never

maintenance_blocker:
  extends: .maintenance
  allow_failure: false
  script: echo "Dummy job to block the rest of the pipeline" && false

update_common:
  extends: .maintenance
  image: golang:$GO_VERSION
  variables:
    TITLE: "Upgrade to common ${COMMON_COMMIT_TAG}"
    TARGET_BRANCH: "$CI_COMMIT_REF_SLUG"
    SOURCE_BRANCH: "upgrade-to-common-$COMMON_COMMIT_TAG-$CI_JOB_ID"
  script: |
     set -x
     git config --global user.email "gitlab-bot@gitlab.com"
     git config --global user.name "GitLab Bot"
     git checkout -b $SOURCE_BRANCH
     go get gitlab.com/gitlab-org/security-products/analyzers/common/v2@${COMMON_COMMIT_TAG}
     go mod tidy
     git diff --quiet go.mod && exit 1
     git commit -m "$TITLE" go.sum go.mod
     git push -o merge_request.create -o merge_request.remove_source_branch -o merge_request.title="$TITLE" -o merge_request.label="backend" -o merge_request.label="devops::secure" -o merge_request.target="$TARGET_BRANCH" ${CI_PROJECT_URL/https:\/\/gitlab.com/https://gitlab-bot:$GITLAB_TOKEN@gitlab.com}.git $SOURCE_BRANCH

.go:
  image: golang:$GO_VERSION
  stage: go

go build:
  extends: .go
  stage: pre-build
  variables:
    CGO_ENABLED: 0
  script:
    - go get ./...
    - CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//')
    - PATH_TO_MODULE=`go list -m`
    - go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o ${CI_PROJECT_DIR}/analyzer
  artifacts:
    paths:
    - analyzer

go test:
  stage: pre-build
  extends: .go
  script:
    - go get ./...
    - go test -race -cover -v ./...
  coverage: '/coverage: \d+.\d+% of statements/'

go lint:
  extends: .go
  stage: pre-build
  script:
    - go get -u golang.org/x/lint/golint
    - golint -set_exit_status ./...

go mod tidy:
  extends: .go
  stage: pre-build
  script:
    - go mod tidy
    - git diff --exit-code go.mod go.sum

build tmp image:
  image: docker:stable
  stage: build-image
  services:
    - docker:19.03.5-dind
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg GO_VERSION -t $TMP_IMAGE .
    - docker push $TMP_IMAGE

# check image size ensures that the total size of the compressed image layers
# is lower than MAX_IMAGE_SIZE_BYTE. This size corresponds to what is
# transferred between the Container Registry of the analyzer
# and the CI when running the scanning job.
check image size:
  stage: test
  variables:
    PATH_NAME: "tmp"
  image: alpine:3.9
  script:
    - apk add --no-cache jq
    - "repository_id=$(wget -q -O- --header \"PRIVATE-TOKEN: $GITLAB_TOKEN\" \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories\" | jq --arg NAME ${PATH_NAME} '.[] | select(.name == $NAME) | .id')"
    - "current_size=$(wget -q -O- --header \"PRIVATE-TOKEN: $GITLAB_TOKEN\" \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${repository_id}/tags/${CI_COMMIT_SHA}\" | jq '.total_size')"
    - echo total size of the compressed image layers, in bytes
    - echo $current_size
    - if [ -n "${MAX_IMAGE_SIZE_MB}" ]; then export MAX_IMAGE_SIZE_BYTE=$(expr $MAX_IMAGE_SIZE_MB \* 1024 \* 1024); fi
    - allowed_image_size=${MAX_IMAGE_SIZE_BYTE:-419430400}
    - echo $allowed_image_size
    - test $allowed_image_size -gt $current_size

check analyzer version:
  stage: test
  image: docker:stable
  stage: test
  services:
    - docker:19.03.5-dind
  script:
    - ANALYZER_VERSION_STRING=$(docker run ${TMP_IMAGE} /analyzer --version || true)
    - CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "${CI_PROJECT_DIR}/CHANGELOG.md" | sed 's/## v//')
    - ANALYZER_VERSION=$(echo "$ANALYZER_VERSION_STRING" | sed -E -n -e 's/^\S+ \S+ (\d+\.\d+\.\d+)/\1/p')
    - infomsg="Please update the analyzer binary version to match the most recent version in CHANGELOG.md."
    - |
      if [ "$ANALYZER_VERSION" == "0.0.0" ] || echo "$ANALYZER_VERSION_STRING" | grep -q "flag provided but not defined: -version"; then
        echo "Warning: analyzer binary does not have a version configured. $infomsg"
      elif [ "$CHANGELOG_VERSION" != "$ANALYZER_VERSION" ]; then
        echo "Error: The most recent version in CHANGELOG.md '$CHANGELOG_VERSION' does not match the analyzer version value '$ANALYZER_VERSION'. $infomsg"
        exit 1
      else
        echo "Success: Analyzer binary version '$ANALYZER_VERSION' matches CHANGELOG.md version '$CHANGELOG_VERSION'"
      fi

test-custom-ca-bundle:
  rules: *downstream_rules
  stage: test
  variables:
    BASE_IMAGE: $CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA
    TARGET: $CI_PROJECT_NAME
    SECURE_LOG_LEVEL: debug
  trigger:
    project: "gitlab-org/security-products/tests/custom-ca"
    branch: master
    strategy: depend

# the following job ensures that the duration of the analyzer scan is below
# the MAX_SCAN_DURATION_SECONDS value. This job must be part of a stage that
# occurs after the `integration test` job, since it determines the scanner
# duration time from the scan that happens as part of the `integration test`
check analyzer scan duration:
  stage: performance-metrics
  image: alpine:3.9
  script:
    - scan_duration=$(cat analyzer_scan_duration_in_seconds.txt)
    - echo $scan_duration
    - allowed_scan_duration=${MAX_SCAN_DURATION_SECONDS:-60}
    - echo $allowed_scan_duration
    - test $allowed_scan_duration -gt $scan_duration
  dependencies:
    - integration test
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - exists:
        - test/expect/*
        - test/fixtures/*

integration test:
  image: docker:stable
  stage: test
  services:
    - docker:19.03.5-dind
  script:
    - REPORT_FILENAME=${REPORT_FILENAME:-gl-sast-report.json}
    - apk update && apk add jq
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - START_TIME=`date +%s`
    - docker run --volume ${CI_PROJECT_DIR}/test/fixtures:/tmp/project --env CI_PROJECT_DIR=/tmp/project --env DS_EXCLUDED_PATHS --env SAST_EXCLUDED_PATHS --env SECRET_DETECTION_EXCLUDED_PATHS $TMP_IMAGE /analyzer run
    - echo $((`date +%s` - $START_TIME)) > analyzer_scan_duration_in_seconds.txt
    - echo "If this integration fails and you believe the expected report is wrong, then patch it using the following diff."
    - echo "$ cat report.diff | patch -Np1 test/expect/$REPORT_FILENAME && git commit -m 'Update expectation' test/expect/$REPORT_FILENAME"
    - wget https://gitlab.com/gitlab-org/security-products/ci-templates/-/raw/master/scripts/compare_reports.sh
    - sh ./compare_reports.sh it test/fixtures/$REPORT_FILENAME test/expect/$REPORT_FILENAME
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - exists:
        - test/expect/*
        - test/fixtures/*
  artifacts:
    paths:
    - analyzer_scan_duration_in_seconds.txt

.docker_tag:
  image: docker:stable
  stage: release-version
  services:
    - docker:19.03.5-dind
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export SOURCE_IMAGE=$TMP_IMAGE
    - export TARGET_IMAGE=$CI_REGISTRY_IMAGE:${IMAGE_TAG:-$CI_JOB_NAME}
    - docker pull $SOURCE_IMAGE
    - docker tag $SOURCE_IMAGE $TARGET_IMAGE
    - docker push $TARGET_IMAGE

.qa-downstream-ds:
  rules: *downstream_rules
  stage: test
  variables:
    SAST_DISABLED: "true"
    CONTAINER_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    SAST_DEFAULT_ANALYZERS: ""
    DS_DEFAULT_ANALYZERS: ""
    DS_ANALYZER_IMAGES: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    DS_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SECURE_LOG_LEVEL: debug
  trigger:
    project: ""
    branch: master
    strategy: depend

.qa-downstream-sast:
  rules: *downstream_rules
  stage: test
  variables:
    DEPENDENCY_SCANNING_DISABLED: "true"
    CONTAINER_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    DS_DEFAULT_ANALYZERS: ""
    SAST_DEFAULT_ANALYZERS: ""
    SAST_ANALYZER_IMAGES: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SAST_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SAST_DISABLE_DIND: "false"
    SECURE_LOG_LEVEL: debug
  trigger:
    project: ""
    branch: master
    strategy: depend

.qa-downstream-cs:
  rules: *downstream_rules
  stage: test
  variables:
    SAST_DISABLED: "true"
    DEPENDENCY_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    SECURE_LOG_LEVEL: debug
  trigger:
    project: ""
    branch: master
    strategy: depend

tag branch:
  extends: .docker_tag
  variables:
    # CAUTION: by preferring `SLUG` over `NAME` we can properly handle non-alphanumeric
    # characters, but this may limit our tags to 63chars or raise potential conflicts.
    IMAGE_TAG: $CI_COMMIT_REF_SLUG
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME
      when: never
    - if: $CI_COMMIT_BRANCH

tag edge:
  extends: .docker_tag
  variables:
    IMAGE_TAG: edge
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME

tag version:
  extends: .docker_tag
  before_script:
    - export IMAGE_TAG=${CI_COMMIT_TAG/v/}
    - echo "Checking that $CI_COMMIT_TAG is last in the changelog"
    - test "$(grep '^## v' CHANGELOG.md |head -n 1)" = "## $CI_COMMIT_TAG"
  rules:
    - if: $CI_COMMIT_TAG

.release:
  extends: .docker_tag
  stage: release-major
  rules:
    - if: $CI_COMMIT_TAG

major:
  extends: .release
  variables:
    IMAGE_TAG: $MAJOR

latest:
  extends: .release

include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'
  - template: SAST.gitlab-ci.yml
  - template: Container-Scanning.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/tmp

benchmark:
  stage: post
  trigger: gitlab-org/security-products/sast-benchmark
  rules:
    # - Only run for SAST jobs on tagged releases
    # - Only run for gitlab-org MRs, as downstream needs exec permissions
    - if: '$REPORT_FILENAME == "gl-sast-report.json" && $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "gitlab-org/security-products/analyzers"'
    - when: never

danger-review:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:danger
  stage: test
  rules:
    - if: '$DANGER_DISABLED == "true"'
      when: never
    - if: '$CI_COMMIT_BRANCH == null'
      when: never
    - <<: *if-gitlab-org-branch
    - when: never
  variables:
    DANGER_BOT_VERSION: v0.0.2
    DANGER_BOT_ANALYZERS_FILE_URL: https://gitlab.com/gitlab-org/security-products/danger-bot/-/raw/${DANGER_BOT_VERSION}/analyzers/Dangerfile
    DANGER_BOT_META_DATA_FILE_URL: https://gitlab.com/gitlab-org/security-products/danger-bot/-/raw/${DANGER_BOT_VERSION}/metadata/Dangerfile
    DANGER_BOT_CHANGELOG_FILE_URL: https://gitlab.com/gitlab-org/security-products/danger-bot/-/raw/${DANGER_BOT_VERSION}/changelog/Dangerfile
    DANGER_BOT_CHANGES_SIZE_FILE_URL: https://gitlab.com/gitlab-org/security-products/danger-bot/-/raw/${DANGER_BOT_VERSION}/changes_size/Dangerfile
  script:
    - git version
    - wget $DANGER_BOT_ANALYZERS_FILE_URL -P .
    - wget $DANGER_BOT_META_DATA_FILE_URL -P danger/metadata/
    - wget $DANGER_BOT_CHANGELOG_FILE_URL -P danger/changelog/
    - wget $DANGER_BOT_CHANGES_SIZE_FILE_URL -P danger/changes_size/
    - danger
