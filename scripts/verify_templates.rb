#!/usr/bin/env ruby

require 'net/http'
require 'json'

LINTER_URI = URI.parse 'https://gitlab.com/api/v4/ci/lint'
STUB = <<YAML
  include:
    - https://gitlab.com/gitlab-org/security-products/ci-templates/raw/#{ENV.fetch('CI_COMMIT_REF_NAME', 'master')}/includes-dev/stub.yml
YAML

def verify(file)
  content = STUB + IO.read(file)
  response = Net::HTTP.post_form(LINTER_URI, content: content)

  file = file.match(/((\w|\+|#)+)\.yml/)[1]

  json = JSON.parse(response.body)
  if json['status'] == 'valid'
    puts "\e[32mvalid\e[0m: #{file}" # Color 'valid' green
  else
    puts "invalid: #{file}"
    puts json['errors']
    exit(1)
  end
end

if ARGV.empty?
  Dir.glob("#{__dir__}/../**/*.yml").each { |file| verify(file) }

  # Given we test all the templates, the coverage is 100%, always. To showcase
  # how this is done, we print it here.
  # Regexp used to parse this: Coverage:\s(\d+)
  puts 'Coverage: 100%'
else
  verify(ARGV[0])
end
